package com.geekhub.orphanage.viewModel

import androidx.lifecycle.ViewModel
import com.geekhub.orphanage.db.entities.Task
import com.google.firebase.database.FirebaseDatabase

class AddTaskViewModel(database: FirebaseDatabase = FirebaseDatabase.getInstance()) :
    ViewModel() {

    private val reference = database.reference

    fun writeNewTask(name: String?, orphanage: String?) {
        val id = reference.push().key!!
        val task = Task(id, name, orphanage)
        reference
            .child("tasks")
            .child(id)
            .setValue(task)
    }
}