package com.geekhub.orphanage.viewModel

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.ViewModel

class ToolsViewModel(activity: Activity?) : ViewModel() {
    val pref: SharedPreferences? = activity!!.getSharedPreferences("pref", Context.MODE_PRIVATE)

    fun altModeOn(pref: SharedPreferences) {
        val editor = pref.edit()
        editor?.putBoolean("pref", true)
        editor?.apply()
    }

    fun altModeOff(pref: SharedPreferences) {
        val editor = pref.edit()
        editor?.putBoolean("pref", false)
        editor?.apply()
    }

}