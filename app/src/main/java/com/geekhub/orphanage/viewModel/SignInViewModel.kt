package com.geekhub.orphanage.viewModel

import androidx.lifecycle.ViewModel
import com.firebase.ui.auth.AuthUI
import com.geekhub.orphanage.R

class SignInViewModel : ViewModel() {
    private val signInProviders =
        arrayListOf(AuthUI.IdpConfig.GoogleBuilder().build())

    val intent = AuthUI.getInstance()
        .createSignInIntentBuilder()
        .setAvailableProviders(signInProviders)
        .setLogo(R.drawable.logo_transparent)
        .build()
}