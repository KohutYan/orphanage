@file:Suppress("DEPRECATION")

package com.geekhub.orphanage.viewModel

import android.app.Application
import android.content.ContentValues
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.geekhub.orphanage.db.entities.MyTask
import com.geekhub.orphanage.db.entities.Task
import com.geekhub.orphanage.db.MainDB
import com.geekhub.orphanage.db.MyTaskDao
import com.geekhub.orphanage.db.MyTaskRepository
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.coroutines.launch

@Suppress("DEPRECATION")
class MyListViewModel(
    application: Application,
    private val database: DatabaseReference = FirebaseDatabase.getInstance().reference,
    private val myTaskDao: MyTaskDao = MainDB.getDB(application).myTaskDao(),
    var repository: MyTaskRepository = MyTaskRepository(myTaskDao)
) : AndroidViewModel(application) {

    val pref: SharedPreferences = application.getSharedPreferences("pref", Context.MODE_PRIVATE)

    val user = FirebaseAuth.getInstance().currentUser

    private var _myTasks = MutableLiveData<MutableList<MyTask>>()
    val myTasks: LiveData<MutableList<MyTask>> get() = _myTasks

    private val connectivityManager =
        application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    private val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
    val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

    fun getMyTasksFromFDB() {
        val taskListener = object : ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                Log.w(ContentValues.TAG, "loadTask:onCancelled", databaseError.toException())
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val taskList = mutableListOf<MyTask>()
                if (dataSnapshot.child("myTasks").child(user?.displayName!!).exists()) {
                    deleteAll()
                    for (snap in dataSnapshot.child("myTasks").child(user.displayName!!).children) {
                        val newTask = MyTask(
                            snap.child("id").value.toString(),
                            snap.child("name").value.toString(),
                            snap.child("orphanage").value.toString()
                        )
                        taskList.add(newTask)
                        _myTasks.postValue(taskList)
                        insert(newTask)
                    }
                } else {
                    _myTasks.postValue(taskList)
                    deleteAll()
                }
            }
        }
        database.addValueEventListener(taskListener)
    }

    fun deleteMyTask(id: String) {
        database.child("myTasks").child(user?.displayName!!).child(id).setValue(null)
    }

    fun toMainList(id: String, name: String?, orphanage: String?) {
        val task = Task(id, name, orphanage)
        database.child("tasks").child(id).setValue(task)
        deleteMyTask(id)

    }

    fun insert(task: MyTask) = viewModelScope.launch {
        repository.insert(task)
    }

    fun deleteAll() = viewModelScope.launch {
        myTaskDao.deleteAll()
    }

    private fun deleteTaskFromRoom(task: MyTask) = viewModelScope.launch {
        myTaskDao.deleteTask(task)
    }
}