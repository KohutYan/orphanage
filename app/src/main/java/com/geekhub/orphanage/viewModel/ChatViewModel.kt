@file:Suppress("DEPRECATION")

package com.geekhub.orphanage.viewModel

import android.app.Application
import android.content.ContentValues
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log
import androidx.lifecycle.*
import com.geekhub.orphanage.db.entities.Message
import com.geekhub.orphanage.db.MainDB
import com.geekhub.orphanage.db.MessageDao
import com.geekhub.orphanage.db.MessageRepository
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.coroutines.launch

@Suppress("DEPRECATION")
class ChatViewModel(
    application: Application,
    val database: DatabaseReference = FirebaseDatabase.getInstance().reference,
    private val msgDao: MessageDao = MainDB.getDB(application).messageDao(),
    val repository: MessageRepository = MessageRepository(msgDao)
) : AndroidViewModel(application) {

    private val _msgs = MutableLiveData<MutableList<Message>>()
    val msgs: LiveData<MutableList<Message>> get() = _msgs

    private val connectivityManager =
        application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    private val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
    val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

    val pref = application.getSharedPreferences("pref", Context.MODE_PRIVATE)

    fun writeNewMessage(text: String?) {
        val id = this.database.push().key!!
        val user = FirebaseAuth.getInstance().currentUser
        val message = Message(id, user?.displayName, text)
        this.database.child("messages").child(id).setValue(message)
    }

    fun getMsgsFromDB() {
        val msgListener = object : ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                Log.w(ContentValues.TAG, "loadPost:onCancelled", databaseError.toException())
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val msgsList = mutableListOf<Message>()
                deleteAll()
                for (snap in dataSnapshot.child("messages").children) {
                    val message = Message(
                        snap.child("id").value.toString(),
                        snap.child("userName").value.toString(),
                        snap.child("messageText").value.toString()
                    )
                    msgsList.add(message)
                    _msgs.postValue(msgsList)
                    insert(message)
                }
            }
        }
        this.database.addValueEventListener(msgListener)
    }

    fun insert(msg: Message) = viewModelScope.launch {
        repository.insert(msg)
    }

    private fun deleteAll() = viewModelScope.launch {
        msgDao.deleteAll()
    }
}