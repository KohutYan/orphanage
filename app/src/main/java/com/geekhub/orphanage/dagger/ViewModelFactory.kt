package com.geekhub.orphanage.dagger

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.geekhub.orphanage.viewModel.ChatViewModel
import com.geekhub.orphanage.viewModel.MainActivityViewModel
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import java.lang.IllegalArgumentException
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton
import kotlin.reflect.KClass

@Suppress("UNCHECKED_CAST")
@Singleton
class ViewModelFactory @Inject constructor(private val application: Application) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T = when {
        modelClass.isAssignableFrom(MainActivityViewModel::class.java) ->
            MainActivityViewModel(application = application) as T

        modelClass.isAssignableFrom(ChatViewModel::class.java) ->
            ChatViewModel(application = application) as T

        else -> throw IllegalArgumentException()
    }

}