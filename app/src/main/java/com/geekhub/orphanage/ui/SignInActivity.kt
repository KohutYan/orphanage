package com.geekhub.orphanage.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.firebase.ui.auth.ErrorCodes
import com.firebase.ui.auth.IdpResponse
import com.geekhub.orphanage.R
import com.geekhub.orphanage.viewModel.SignInViewModel
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_sign_in.*
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask

class SignInActivity : AppCompatActivity() {

    @Suppress("PrivatePropertyName")
    private val RC_SIGN_IN = 1
    val viewModel = SignInViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        signInButton.setOnClickListener {
            val intent = viewModel.intent
            startActivityForResult(intent, RC_SIGN_IN)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            if (resultCode == Activity.RESULT_OK) {
                val user = FirebaseAuth.getInstance().currentUser
                val username = user?.displayName
                startActivity(intentFor<MainActivity>().newTask().clearTask())
                Toast.makeText(this, "You successfully signed in, $username", Toast.LENGTH_LONG)
                    .show()
            } else {
                val response = IdpResponse.fromResultIntent(data)
                when (response?.error?.errorCode) {
                    ErrorCodes.NO_NETWORK ->
                        Toast.makeText(this, "No Network", Toast.LENGTH_LONG).show()
                    ErrorCodes.UNKNOWN_ERROR ->
                        Toast.makeText(this, "Unknown error", Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}