@file:Suppress("DEPRECATION")

package com.geekhub.orphanage.ui

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.firebase.ui.auth.AuthUI
import com.geekhub.orphanage.*
import com.geekhub.orphanage.viewModel.MainActivityViewModel
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.themedToolbar
import org.jetbrains.anko.toolbar
import kotlin.properties.Delegates


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    /*@Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory*/
    //TODO: Add Dagger

    private lateinit var viewModel: MainActivityViewModel
    var currentItemId = R.id.nav_home

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        viewModel = MainActivityViewModel(application)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        val user = FirebaseAuth.getInstance().currentUser
        val header = navView.getHeaderView(0)
        val userName = header.userName
        val userMail = header.userMail
        val userImg = header.profile_image
        userName?.text = user?.displayName.toString()
        userMail?.text = user?.email.toString()
        Picasso.get().load(user?.photoUrl.toString()).into(userImg)

        navView.setNavigationItemSelectedListener(this)

        onNavigationItemSelected(navView.menu.findItem(R.id.nav_home))
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        when {
            drawerLayout.isDrawerOpen(GravityCompat.START) -> {
                drawerLayout.closeDrawer(GravityCompat.START)
            }
            currentItemId != R.id.nav_home -> {
                val navView: NavigationView = findViewById(R.id.nav_view)
                onNavigationItemSelected(navView.menu.findItem(R.id.nav_home))
            }
            else -> {
                super.onBackPressed()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
//         Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_update -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_home -> {
                currentItemId = R.id.nav_home
                val mainListFragment = MainListFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.content_main, mainListFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit()
            }
            R.id.nav_my_list -> {
                currentItemId = R.id.nav_my_list
                val myListFragment = MyListFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.content_main, myListFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit()
            }
            R.id.nav_chat -> {
                currentItemId = R.id.nav_chat
                val chatFragment = ChatFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.content_main, chatFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit()
            }
            R.id.nav_tools -> {
                currentItemId = R.id.nav_tools
                val toolsFragment = ToolsFragment()
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.content_main, toolsFragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit()
            }
            R.id.nav_sign_out -> {
                AuthUI.getInstance()
                    .signOut(this)
                    .addOnCompleteListener {
                        finish()
                        startActivity<SignInActivity>()
                    }
            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("currentItemId", currentItemId)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        val navView: NavigationView = findViewById(R.id.nav_view)
        onNavigationItemSelected(navView.menu.findItem(savedInstanceState.getInt("currentItemId")))
    }
}
