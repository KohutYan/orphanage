@file:Suppress("DEPRECATION")

package com.geekhub.orphanage

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.geekhub.orphanage.adapters.MainListAdapter
import com.geekhub.orphanage.ui.AddTaskActivity
import com.geekhub.orphanage.viewModel.MainActivityViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jetbrains.anko.support.v4.startActivity


class MainListFragment : Fragment() {

    companion object {
        fun newInstance() = MainListFragment()
    }

    lateinit var adapter: MainListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_main, container, false)
        adapter = MainListAdapter(mutableListOf(), context = activity!!.applicationContext)
        val viewModel = MainActivityViewModel(activity!!.application)
        adapter.isConnected = viewModel.isConnected

        val mainList = rootView.findViewById(R.id.mainList) as RecyclerView
        mainList.layoutManager = LinearLayoutManager(activity)
        mainList.adapter = adapter

        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val viewModel = MainActivityViewModel(activity!!.application)

        val isConnected = viewModel.isConnected

        val pref = viewModel.pref
        val prefValue = pref.getBoolean("pref", false)

        CoroutineScope(Dispatchers.Default).launch {
            viewModel.getTasksFromDB()
        }

        adapter.onItemHold = { task ->
            if (isConnected) {
                val dialogBuilder = AlertDialog.Builder(activity)
                dialogBuilder
                    .setMessage(getString(R.string.delete_question))
                    .setCancelable(true)
                    .setPositiveButton("Yes") { _, _ ->
                        viewModel.deleteTask(task.id)
                        Toast.makeText(
                            activity!!.applicationContext,
                            getString(
                                if (prefValue)
                                    R.string.item_deleted_meme
                                else
                                    R.string.item_deleted
                            ),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    .setNegativeButton("No") { dialog, _ ->
                        dialog.cancel()
                    }
                val alert = dialogBuilder.create()
                alert.setTitle(getString(R.string.delete))
                alert.show()

            } else {
                Toast.makeText(
                    activity!!.applicationContext,
                    getString(
                        if (prefValue)
                            R.string.no_network_meme
                        else
                            R.string.no_network
                    ),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        adapter.onItemClick = { task ->
            if (isConnected) {
                viewModel.toMyList(task.id, task.name, task.orphanage)
                adapter.notifyItemRemoved(0)
                Toast.makeText(
                    activity!!.applicationContext,
                    getString(
                        if (prefValue)
                            R.string.to_my_list_meme
                        else
                            R.string.to_my_list
                    ),
                    Toast.LENGTH_SHORT
                ).show()

            } else {
                Toast.makeText(
                    activity!!.applicationContext,
                    getString(
                        if (prefValue)
                            R.string.no_network_meme
                        else
                            R.string.no_network
                    ),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        if (isConnected) {
            viewModel.tasks.observe(viewLifecycleOwner, Observer {
                adapter.setAll(it)
            })
        } else {
            viewModel.repository.allTasks.observe(viewLifecycleOwner, Observer {
                adapter.setAll(it)
            })
        }

        val fab: FloatingActionButton = fab
        fab.setOnClickListener {
            if (isConnected) {
                startActivity<AddTaskActivity>()
            } else {
                Toast.makeText(
                    activity!!.applicationContext,
                    getString(
                        if (prefValue)
                            R.string.no_network_meme
                        else
                            R.string.no_network
                    ),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
}
