package com.geekhub.orphanage

import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import com.geekhub.orphanage.viewModel.ToolsViewModel
import kotlinx.android.synthetic.main.fragment_tools.*

/**
 * A simple [Fragment] subclass.
 */
class ToolsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_tools, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val viewModel = ToolsViewModel(activity!!)
        val pref = viewModel.pref
        if (pref!!.getBoolean("pref", false)) {
            alternativeModeSwitch.isChecked = true
        }
        alternativeModeSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                viewModel.altModeOn(pref)
            } else {
                viewModel.altModeOff(pref)
            }
        }
    }
}

