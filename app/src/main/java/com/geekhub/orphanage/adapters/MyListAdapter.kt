package com.geekhub.orphanage.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.geekhub.orphanage.R
import com.geekhub.orphanage.db.entities.MyTask
import kotlinx.android.synthetic.main.my_item_row.view.*

class MyListAdapter(private val myTasks: MutableList<MyTask>, val context: Context) :
    RecyclerView.Adapter<MyListAdapter.ViewHolder>() {

    var onMyItemHold: ((MyTask) -> Unit)? = null
    var onMyItemClick: ((MyTask) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.my_item_row, parent, false)
        return ViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        return myTasks.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvTaskName.text = myTasks[position].name
        holder.tvOrphanage.text = myTasks[position].orphanage
    }

    fun setMyList(items: MutableList<MyTask>?) {
        if (items != null) {
            myTasks.clear()
            myTasks.addAll(items)
            notifyDataSetChanged()
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvTaskName = itemView.taskName!!
        val tvOrphanage = itemView.taskOrphanage!!

        init {
            this.itemView.setOnLongClickListener {
                onMyItemHold?.invoke(myTasks[adapterPosition])
                true
            }
            this.itemView.favButton.setOnClickListener {
                onMyItemClick?.invoke(myTasks[adapterPosition])
            }
        }
    }
}