package com.geekhub.orphanage.adapters

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.geekhub.orphanage.R
import com.geekhub.orphanage.db.entities.Task
import kotlinx.android.synthetic.main.item_row.view.*
import kotlin.properties.Delegates

@Suppress("DEPRECATION")
class MainListAdapter(val tasks: MutableList<Task>, val context: Context) :
    RecyclerView.Adapter<MainListAdapter.ViewHolder>() {

    var isConnected by Delegates.notNull<Boolean>()
    var onItemHold: ((Task) -> Unit)? = null
    var onItemClick: ((Task) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.item_row, parent, false)
        return ViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        return tasks.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvTaskName.text = tasks[position].name
        holder.tvOrphanage.text = tasks[position].orphanage
    }

    fun setAll(items: MutableList<Task>?) {
        if (items != null) {
            tasks.clear()
            tasks.addAll(items)
            notifyDataSetChanged()
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvTaskName = itemView.taskName!!
        val tvOrphanage = itemView.taskOrphanage!!
        val drawable: Drawable = context.resources.getDrawable(R.drawable.ic_favorite_pink)

        init {
            itemView.setOnLongClickListener {
                onItemHold?.invoke(tasks[adapterPosition])
                true
            }
            this.itemView.favButton.setOnClickListener {
                if (isConnected) {
                    itemView.favButton.setImageDrawable(drawable)
                }
                onItemClick?.invoke(tasks[adapterPosition])
            }
        }
    }
}

