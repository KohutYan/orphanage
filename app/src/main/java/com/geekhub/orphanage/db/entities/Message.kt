package com.geekhub.orphanage.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
@Entity(tableName = "messages")
data class Message(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "message_id")
    val id: String = "1",
    @ColumnInfo(name = "user_name")
    val userName: String? = "name",
    @ColumnInfo(name = "message_text")
    val messageText: String? = "Text"
){
    @Exclude
    fun toMap(): Map<String, Any?>{
        return mapOf(
            "id" to id,
            "userName" to userName,
            "messageText" to messageText
        )
    }
}