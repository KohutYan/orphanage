package com.geekhub.orphanage.db

import androidx.lifecycle.LiveData
import com.geekhub.orphanage.db.entities.Message

class MessageRepository(private val messageDao: MessageDao) {
    val allMessages: LiveData<MutableList<Message>> = messageDao.getMsgsFromDao()

     suspend fun insert(msg: Message) {
        messageDao.insert(msg)
    }
}