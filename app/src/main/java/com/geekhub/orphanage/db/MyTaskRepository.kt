package com.geekhub.orphanage.db

import androidx.lifecycle.LiveData
import com.geekhub.orphanage.db.entities.MyTask

class MyTaskRepository(private val myTaskDao: MyTaskDao) {
    val allTasks: LiveData<MutableList<MyTask>> = myTaskDao.getTasksFromDao()

    suspend fun insert(task: MyTask) {
        myTaskDao.insert(task)
    }
}