package com.geekhub.orphanage.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.geekhub.orphanage.db.entities.Task

@Dao
interface TaskDao {
    @Query("SELECT * from tasks")
    fun getTasksFromDao(): LiveData<MutableList<Task>>

    @Query("SELECT * FROM tasks WHERE task_id =:id")
    fun getTask(id: String): Task

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(task: Task)

    @Query("DELETE FROM tasks")
    suspend fun deleteAll()

    @Delete
    suspend fun deleteTask(task: Task)
}