package com.geekhub.orphanage.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.geekhub.orphanage.db.entities.Message
import com.geekhub.orphanage.db.entities.MyTask
import com.geekhub.orphanage.db.entities.Task

@Database(entities = [Task::class, Message::class, MyTask::class], version = 3, exportSchema = false)
abstract class MainDB : RoomDatabase() {

    abstract fun taskDao(): TaskDao
    abstract fun messageDao(): MessageDao
    abstract fun myTaskDao(): MyTaskDao

    companion object {
        @Volatile
        private var INSTANCE: MainDB? = null

        fun getDB(context: Context): MainDB {
            val tempInstance = INSTANCE
            if (INSTANCE != null) {
                return tempInstance!!
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    MainDB::class.java,
                    "task_database"
                ).fallbackToDestructiveMigration().build()
                INSTANCE = instance
                return instance
            }
        }
    }

}