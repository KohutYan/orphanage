package com.geekhub.orphanage.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.geekhub.orphanage.db.entities.Message

@Dao
interface MessageDao {
    @Query("SELECT * from messages")
    fun getMsgsFromDao(): LiveData<MutableList<Message>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(msg: Message)

    @Query("DELETE FROM messages")
    suspend fun deleteAll()
}