package com.geekhub.orphanage.db

import androidx.lifecycle.LiveData
import com.geekhub.orphanage.db.entities.Task

class TaskRepository(private val taskDao: TaskDao) {
    val allTasks: LiveData<MutableList<Task>> = taskDao.getTasksFromDao()

    suspend fun insert(task: Task) {
        taskDao.insert(task)
    }
}