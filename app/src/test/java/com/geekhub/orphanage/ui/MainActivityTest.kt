package com.geekhub.orphanage.ui

import com.geekhub.orphanage.R
import com.google.android.material.navigation.NavigationView
import io.mockk.mockk
import io.mockk.spyk
import kotlinx.android.synthetic.main.activity_main.*
import org.junit.Test

import org.junit.Assert.*
import org.mockito.Mock
import org.mockito.Spy

class MainActivityTest {

    val activity = MainActivity()

    @Test
    fun onCreate() {
        val current = activity.currentItemId
        assertEquals(R.id.nav_home, current) //asserts that start page is nav_home
    }
}