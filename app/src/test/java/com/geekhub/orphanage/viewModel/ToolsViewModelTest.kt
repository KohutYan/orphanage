package com.geekhub.orphanage.viewModel

import android.app.Activity
import android.content.SharedPreferences
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ToolsViewModelTest {
    lateinit var viewModel: ToolsViewModel

    @Mock
    lateinit var activity: Activity

    @Mock
    lateinit var mockedSP: SharedPreferences

    @Mock
    lateinit var editor: SharedPreferences.Editor

    @Before
    fun before() {
        viewModel = ToolsViewModel(activity)
        doReturn(mockedSP).whenever(activity).getSharedPreferences(any(), any())
        doReturn(editor).whenever(mockedSP).edit()
        doReturn(false).whenever(mockedSP).getBoolean(any(), any())
    }

    @Test
    fun should_return_def_value() {
        val bool = mockedSP.getBoolean("pref", false)

        assertEquals(bool, false)
    }

    @Test
    fun should_verify_altModeOn_method() {
        //given
        val sharedPreferences = activity.getSharedPreferences("pref", 0)
        //when
        viewModel.altModeOn(sharedPreferences)
        //then
        verify(editor).putBoolean("pref", true)
    }

    @Test
    fun should_verify_altModeOff_method() {
        //given
        val sharedPreferences = activity.getSharedPreferences("pref", 0)
        //when
        viewModel.altModeOff(sharedPreferences)
        //then
        verify(editor).putBoolean("pref", false)
    }

    @Test
    fun should_verify_altModeOn_apply() {
        //given
        val sharedPreferences = activity.getSharedPreferences("pref", 0)
        //when
        viewModel.altModeOn(sharedPreferences)
        //when
        verify(editor).apply()
    }

}