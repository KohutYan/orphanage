package com.geekhub.orphanage.viewModel

import android.app.Application
import com.geekhub.orphanage.db.MainDB
import com.geekhub.orphanage.db.MessageDao
import com.geekhub.orphanage.db.MessageRepository
import com.geekhub.orphanage.db.entities.Message
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ChatViewModelTest {

    lateinit var mockedDao: MessageDao
    lateinit var mockedDB: MainDB
    lateinit var mockedRepo: MessageRepository
    lateinit var viewModel: ChatViewModel
    lateinit var application: Application
    lateinit var databaseReference: DatabaseReference
    lateinit var database: FirebaseDatabase

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        application = mock(Application::class.java)
        mockedDB = mock(MainDB::class.java)
        mockedRepo = mock(MessageRepository::class.java)
        databaseReference = mock(DatabaseReference::class.java)
        database = mock(FirebaseDatabase::class.java)
        viewModel = mock(ChatViewModel::class.java)
        mockedDao = mock(MessageDao::class.java)
    }

    @Test
    fun insert() =
        runBlocking {
            val message = Message("1", "name", "text")
            //doReturn(message).whenever(mockedRepo).insert(message)

            viewModel.insert(message)

            verify(mockedRepo).insert(message)

    }
}