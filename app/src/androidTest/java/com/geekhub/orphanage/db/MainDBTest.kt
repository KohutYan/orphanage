package com.geekhub.orphanage.db

import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import com.geekhub.orphanage.db.entities.MyTask
import com.geekhub.orphanage.db.entities.Task
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class MainDBTest {

    private var taskDao: TaskDao? = null
    private var myTaskDao: MyTaskDao? = null

    @Before
    fun setup() {
        taskDao = MainDB.getDB(InstrumentationRegistry.getInstrumentation().targetContext).taskDao()
        myTaskDao = MainDB.getDB(InstrumentationRegistry.getInstrumentation().targetContext).myTaskDao()
    }

    @After
    fun tearDown() {

    }

    @Test
    fun should_insert_tasks_items() {
        val task = Task("1", "a", "b")
        runBlocking { taskDao?.insert(task) }
        val taskTest = taskDao?.getTask(task.id)
        assertEquals(task, taskTest)
    }

    @Test
    fun should_delete_tasks_items() {
        val task = Task("1", "a", "b")
        runBlocking { taskDao?.insert(task) }
        runBlocking { taskDao?.deleteTask(task) }
        val delTest: Task? = taskDao?.getTask(task.id)
        assertNull(delTest)
    }

    @Test
    fun should_take_not_null_task_list_then_del_task(){
        val task = Task("1", "a", "b")
        runBlocking { taskDao?.insert(task) }
        val taskList = runBlocking { taskDao?.getTasksFromDao() }
        assertNotNull(taskList)
        runBlocking { taskDao?.deleteTask(task) }
    }

    @Test
    fun should_insert_my_task(){
        val task = MyTask("1", "a", "b")
        runBlocking { myTaskDao?.insert(task) }
        val myTaskTest = myTaskDao?.getMyTask(task.id)
        assertEquals(myTaskTest, task)
    }

    @Test
    fun should_delete_my_task(){
        val task = MyTask("1", "a", "b")
        runBlocking { myTaskDao?.insert(task) }
        runBlocking { myTaskDao?.deleteTask(task) }
        val delTest = myTaskDao?.getMyTask(task.id)
        assertNull(delTest)
    }

    @Test
    fun should_take_not_null_my_task_list_then_del_task(){
        val task = MyTask("1", "a", "b")
        runBlocking { myTaskDao?.insert(task) }
        val taskList = runBlocking { myTaskDao?.getTasksFromDao() }
        assertNotNull(taskList)
        runBlocking { myTaskDao?.deleteTask(task) }
    }

}